package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.IngresarSteps;
import com.choucair.formacion.steps.SimulacionSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SimulacionDefinition {

	@Steps
	IngresarSteps ingresarSteps;

	@Steps
	SimulacionSteps simulacionSteps;

	@Given("^Ingreso a la pantalla simular$")
	public void ingreso_a_la_pantalla_simular() throws Throwable {
		ingresarSteps.IngresarPantallaSimulacion();
		ingresarSteps.ProductosServicios();
	}

	@Given("^Ingreso los campos en el formulario$")
	public void ingreso_los_campos_en_el_formulario(DataTable dtDatosForms) throws Throwable {
		List<List<String>> data = dtDatosForms.raw();

		for (int i = 1; i < data.size(); i++) {
			simulacionSteps.diligenciar_form_simulacion(data, i);

		}
	}

	@When("^Doy click en el boton validar$")
	public void doy_click_en_el_boton_validar() throws Throwable {
		simulacionSteps.Simular();
	}

	@Then("^Verifico que se presente el resultado$")
	public void verifico_que_se_presente_el_resultado() throws Throwable {
		simulacionSteps.verificar_ingreso_datos_formulario_exitoso();
	}
	
	@Then("^Verifico que se presente validacion$")
	public void verifico_que_se_presente_validacion() throws Throwable {
		simulacionSteps.verificar_ingreso_datos_formulario_con_errores();
	}

}
