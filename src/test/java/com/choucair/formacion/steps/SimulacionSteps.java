package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.SimulacionPage;

import net.thucydides.core.annotations.Step;

public class SimulacionSteps {
	SimulacionPage simulacionPage;

	@Step
	public void diligenciar_form_simulacion(List<List<String>> data, int id) {
		simulacionPage.ValorActivo(data.get(id).get(0).trim());
		simulacionPage.Plazo(data.get(id).get(1).trim());
		simulacionPage.Opcion_Compra(data.get(id).get(2).trim());
		simulacionPage.Tipo_Tasa(data.get(id).get(3).trim());
		simulacionPage.Modalidad(data.get(id).get(4).trim());

	}	

	@Step
	public void Simular() throws InterruptedException {
		simulacionPage.Simular();
	}

	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		simulacionPage.form_sin_errores();
	}

	@Step
	public void verificar_ingreso_datos_formulario_con_errores() {
		simulacionPage.form_con_errores();
	}

}
