package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.IngresarPage;
import com.choucair.formacion.pageobjects.ProdServiciosPage;

import net.thucydides.core.annotations.Step;

public class IngresarSteps 
{
	IngresarPage ingresarPage;
	ProdServiciosPage prodServiciosPage;
	
	@Step
	public void IngresarPantallaSimulacion()
		{
			//a. Abrir navegador con la url de prueba
		ingresarPage.open();
		ingresarPage.VerificaHome();
		}
	
	@Step
	public void ProductosServicios()
	{
		prodServiciosPage.ProductosServicios();
		prodServiciosPage.Leasing();
		prodServiciosPage.LeasingHabitacional();
		prodServiciosPage.CanonConstante();
	}
}
