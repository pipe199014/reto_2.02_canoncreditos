package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;

import com.ibm.icu.text.DecimalFormat;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SimulacionPage extends PageObject {
	static Properties propiedades;
	// Campo valor activo
	@FindBy(name = "valorActivo")
	public WebElementFacade txtValorActivo;
	// Campo plazo
	@FindBy(name = "plazo")
	public WebElementFacade txtPlazo;
	// Campo opcion compra
	@FindBy(name = "opcionCompra")
	public WebElementFacade txtOpcionCompra;
	// Campo tipo tasa porcentaje
	@FindBy(name = "comboTipoTasa")
	public WebElementFacade txtTipo_Tasa;
	// Campo Modalidad
	@FindBy(xpath = "//*[@id=\"sim-detail\"]/form/div[5]/input")
	public WebElementFacade txtModalidad;

	// Boton validar
	@FindBy(xpath = "//*[@id=\"sim-detail\"]/form/div[6]/button")
	public WebElementFacade btnSimular;

	// Verificar Resultado
	@FindBy(xpath = "//*[@id=\"resultado\"]/h2")
	public WebElementFacade lblResultado;
	
	//Campo validación
	@FindBy(xpath = "//*[@id=\"sim-detail\"]/form/div[1]/div/div/span[3]")
	public WebElementFacade lblMensajeVali;
	
	
	//////////////////////////// Propiedades
	//////////////////////////// /////////////////////////////////////////
	public void ValorActivo(String datoPrueba) {
		txtValorActivo.click();
		txtValorActivo.clear();
		txtValorActivo.sendKeys(datoPrueba);
		tiempo(1000);
	}

	public void Plazo(String datoPrueba) {
		txtPlazo.click();
		txtPlazo.clear();
		txtPlazo.sendKeys(datoPrueba);
		tiempo(1000);
	}

	public void Opcion_Compra(String datoPrueba) {
		txtOpcionCompra.click();
		txtOpcionCompra.clear();
		txtOpcionCompra.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Tipo_Tasa(String datoPrueba) {
		txtTipo_Tasa.click();
		txtTipo_Tasa.sendKeys(datoPrueba);
		tiempo(1000);

	}

	public void Modalidad(String datoPrueba) {
		String textMod = "Vencida";
		String strMensaje = txtModalidad.getTextValue();
		assertThat(strMensaje, containsString(textMod));

	}

	public void Simular() {
		btnSimular.click();
		tiempo(1000);
	}

	public void tiempo(int tiempo) {

		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/////////////////////////////////// Campos de
	/////////////////////////////////// validación///////////////////////////////////////

	@FindBy(xpath = "//*[@id=\"resultado\"]/div/table/tbody/tr")
	public List<WebElement> tblFila;

	public void form_sin_errores() {

		// Verifico que muestre tabla
		String lblResSimulacion = "Resultado de la simulación";
		String strMensaje = lblResultado.getText();
		assertThat(strMensaje, containsString(lblResSimulacion));

		////////////////////////////////////

		for (WebElement trElement : tblFila) {
			List<WebElement> tblColumna = trElement.findElements(By.xpath("td"));
			int cont = 0;
			for (WebElement tdElement : tblColumna) {

				if (cont == 1)
					System.out.println(tdElement.getText());

				cont++;
			}
		}
	}

	public void form_con_errores() {
		// Verifico que muestre mensaje de validación
		assertThat(lblMensajeVali.isCurrentlyVisible(), is(true));

	}
}
