package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class IngresarPage extends PageObject {
	
	// Label Simulacion
			@FindBy(xpath = "//*[@id=\"main-menu\"]/div[2]/ul[1]/li[3]/a")
			public WebElementFacade lblProdServicios;
			

			public void VerificaHome() {
				String labelv = "Productos y Servicios";
				String strMensaje = lblProdServicios.getText();
				assertThat(strMensaje, containsString(labelv));
			}
			

}
