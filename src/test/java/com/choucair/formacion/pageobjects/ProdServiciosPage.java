package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;

import com.ibm.icu.text.DecimalFormat;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProdServiciosPage extends PageObject {
	static Properties propiedades;

	// Link Productos y servicios
	@FindBy(xpath = "//*[@id=\"main-menu\"]/div[2]/ul[1]/li[3]/a")
	public WebElementFacade lblProdServicios;
	// Link Leasing
	@FindBy(xpath = "//*[@id=\"productosPersonas\"]/div/div[1]/div/div/div[2]/div/a")
	public WebElementFacade lnkLeasing;
	// Link LeasingHabitacional
	@FindBy(xpath = "//*[@id=\"category-detail\"]/div/div/div[2]/div/div[2]/h2/a")
	public WebElementFacade lnkLeasingHabitacional;
	// Link Canon Constante
	@FindBy(xpath = "//*[@id=\"main-content\"]/div[4]/div/div/div/div/div[1]/div/div/div[1]/a")
	public WebElementFacade lnkCanonConstante;
	
	
	public void ProductosServicios() {
		lblProdServicios.click();
		tiempo(1000);
	}

	public void Leasing() {
		lnkLeasing.click();
		tiempo(1000);
	}
	
	public void LeasingHabitacional() {
		lnkLeasingHabitacional.click();
		tiempo(1000);
	}

	public void CanonConstante() {
		lnkCanonConstante.click();
		tiempo(1000);
	}
	
	public void tiempo(int tiempo) {

		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
