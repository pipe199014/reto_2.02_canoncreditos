@Regresion
Feature: Verificar el funcionamiento de la pantalla de simulacion de canon financiero por el grupo Bancolombia, 
en cuanto a la presentacion de los valores calculados.

  @CasoExitoso
  Scenario: Verificar el funcionamiento del simulador de calcular y 
  presentar el resultado en la tabla
    Given Ingreso a la pantalla simular
    And Ingreso los campos en el formulario
     | Valor_Activo | Plazo    | Opcion_Compra | Tipo_Tasa | Modalidad |
     | 20000000     | 36       | 5             | IPC       | Vencida   |
      
    When Doy click en el boton validar   
    Then Verifico que se presente el resultado
    
     @CasoAlterno
  Scenario: Verificar que se presente mensaje del validacion en el campo valor activo cuando sea un valor menor a $10.000.000
    Given Ingreso a la pantalla simular
    And Ingreso los campos en el formulario
     | Valor_Activo | Plazo    | Opcion_Compra | Tipo_Tasa | Modalidad |
     | 2000000    | 36       | 5             | IPC       | Vencida   |
     
    Then Verifico que se presente validacion
   

 